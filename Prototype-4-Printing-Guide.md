This is a guide for 3D printing parts for the LucidVR glove prototype.  
These instructions are meant for FDM printers. This should apply to almost any FDM printer.  

**Recommended Nozzle Diameter: 0.4**  

These parts are specifically designed to **not** need supports so do not turn on support structures unless you need to.  

Before printing your parts make sure that you print the right ones for your hardware. WL Potentiometers and Green potentiometers are both supported.  
![Green Pots vs WL Pots](https://cdn.discordapp.com/attachments/785135646082990120/875850313578459176/unknown.png)  
This will affect which Tensioner and which Holder part you will print.

The holders you print will also be determined by the mounting hardware you use. There are two options for mounting:
Legacy Mounting, which uses elastic bands like prototype 3.
Rigid Mount, which adds a new modular slide mount system.

![Rigid Mount](https://cdn.discordapp.com/attachments/785135646082990120/875859787257102417/unknown.png)
![Elastic Mounts](https://cdn.discordapp.com/attachments/785135646082990120/875864994686844998/unknown.png)

All parts for prototype 4 are designed to be printed flat. Print them in the following orientations for the best finish:  

![Recommended orientations](https://cdn.discordapp.com/attachments/785135646082990120/875867269270495252/unknown.png)  

STL files for 3D printing are located in the hardware folder. 

The assembly **for each hand** is as follows:
* Spool (5x)
* Tensioner (5x) (green or WL)
* Cover (5x)
* Holder (5x) (Green or WL, elastic or rigid mount)
* GuideNode(3.1) (2+ per finger, 1+ for thumb) (can substitute with rings from proto 3)
* EndCap (1x per finger) 

Recommended settings:
* Infill: 10-20%, you can go higher if your layer adhesion needs it
* Layer height: 0.2mm
* No supports (brims are okay)
* Shell thickness: 0.8mm (2 outlines)
* Retraction settings: May differ between printers, 5mm on Ender 3 is fine


Disclaimer: You WILL need to print multiple sizes of the EndCap g to fit the size of your fingers. Resize these in your slicer. Alternatively, you could print them in TPU for more flexible sizing. I recommend resizing the EndCaps non-uniformly to squish them down to fit the oval shape of your fingers, this way they don't rotate around.

![Different sized end caps](https://cdn.discordapp.com/attachments/822593475396632587/823344405712601149/unknown.png)

If your bed is big and level enough, you may be able to print all the parts at once, but feel free to print them individually as well, you may get higher yield printing individually.  
![All parts at once](https://cdn.discordapp.com/attachments/785135646082990120/875878026062204928/unknown.png)
