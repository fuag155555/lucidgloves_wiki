Welcome to the lucidgloves-hardware wiki!

**[Parts Lists:](https://github.com/LucidVR/lucidgloves/wiki/Parts-Lists)**
* [Prototype 3](https://github.com/LucidVR/lucidgloves/wiki/Prototype-3.1-Parts-List)
* [Prototype 4](https://github.com/LucidVR/lucidgloves/wiki/Prototype-4-Parts-List)

**[Printing guides:](https://github.com/LucidVR/lucidgloves/wiki/Parts-Printing-Guide)**
* [Prototype 3](https://github.com/LucidVR/lucidgloves/wiki/Prototype-3-Printing-Guide)
* [Prototype 4](https://github.com/LucidVR/lucidgloves/wiki/Prototype-4-Printing-Guide)

**Wiring Diagrams:**
* [Prototype 3](https://github.com/LucidVR/lucidgloves/wiki/Prototype-3-Wiring-Diagram)
* Prototype 4 (WIP)

**[Build Guides:](https://github.com/LucidVR/lucidgloves/wiki/Build-Guides)**
* [Prototype 3](https://github.com/LucidVR/lucidgloves/wiki/Prototype-3-Building-Tutorial)
* Prototype 4 (WIP)

**[Firmware Customization V2](https://github.com/LucidVR/lucidgloves/wiki/Firmware-V2-Customization)**